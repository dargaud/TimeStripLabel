#ifndef __TIME_STRIP_LABEL_H
#define __TIME_STRIP_LABEL_H

#include <time.h>

// Those are the different kinds of intervals. They can be guessed with TSL_Auto
enum { TSL_NONE, 	// No label
	   TSL_SEC, 	// One label every second
	   TSL_3SEC,	// One label every 3s
	   TSL_10SEC,	// One label every 10s
	   TSL_MIN,		// One label every minute
	   TSL_3MIN,	// One label every 3min
	   TSL_10MIN,	// One label every 10min
	   TSL_HOUR,	// One label every hour
	   TSL_2HOUR,	// One label every 6h
	   TSL_6HOUR,	// One label every 6h
	   TSL_DAY,		// One label every day
	   TSL_2DAY,	// One label every 2 days
	   TSL_WEEK,	// One label every week
	   TSL_MONTH,	// One label every month
	   TSL_TRIM,	// One label every trimester
	   TSL_YEAR	};	// One label every year
// Note that the above are not exactly time intervals but rather time thresholds
// i.e. if using TSL_10MIN, you will get a new label every time the time passes HH:00:00, HH:10:00, ... HH:50:00

// Min time between labels, in s. This is approximate (see Adj in Auto function)
extern int   TSL_MTBL[];
extern char *TSL_Label[];

// Minimalist format strings for the intervals above. You can change them.
extern char *TSL_FormatMinimal[];
extern char *TSL_AxisNameMinimal[];

// Another more complete set of format strings for the intervals above.
// You can change them or pass whatever you want
extern char *TSL_FormatTimeDate[];
extern char *TSL_AxisNameTimeDate[];

extern int TSL_Auto(int panel, int control, double Rate, char *frmt[], double Adj);

extern int TSL_PlotStripChartPoint(int panel, int control, double Y, time_t Time, int Unit, char *frmt);

extern int TSL_PlotStripChart(int panel, int control, void* Y, int Nb, int Start, int Skip, int yDataType,
		time_t Time, int Unit, char *frmt);

extern int  TSL_Reset(int panel, int control);

extern void TSL_Free(void);

#endif
