///////////////////////////////////////////////////////////////////////////////
// MODULE	TimeStripLabel
// PURPOSE	Label the axis of a strip chart with arbitrary date/time 
//			even if the rate of the data is non-deterministic
// (c) 2012 Guillaume Dargaud - http://www.gdargaud.net/Hack/LabWindows.html
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>		
#include <time.h>		

#include <cvirte.h>		
#include <userint.h>
#include <toolbox.h>

#include "iso646.h"
#include "Def.h"

//#define TSL_DEBUG	// Use this for testing as a standalone program with TimeStripLabelTest.uir. Disable for a module/lib

#include "TimeStripLabel.h"

#ifdef TSL_DEBUG
	static int Pnl;
	#include "TimeStripLabelTest.h"
#endif



// Min time between labels, in s. This is approximate (see Adj in Auto function)
int TSL_MTBL[]={	0,
					1,				// 3 times more frequent than the next one
					3,				// 3 times more frequent than the next one
					10,				// 6 times more frequent than the next one
					60,				// 3 times more frequent than the next one
					60*3,			// 3 times more frequent than the next one
					60*10,			// 6 times more frequent than the next one
					60*60,			// 2 times more frequent than the next one
					60*60*2,		// 3 times more frequent than the next one
					60*60*6,		// 4 times more frequent than the next one
					60*60*24,		// 2 times more frequent than the next one
					60*60*24*2,		// 3 times more frequent than the next one
					60*60*24*7,		// 4 times more frequent than the next one
					60*60*24*30,	// 3 times more frequent than the next one
					60*60*24*30*3,	// 4 times more frequent than the next one
					60*60*24*365	};

char *TSL_Label[]={	"none",
					"1s",
					"3s",
					"10s",
					"1min",
					"3min",
					"10min",
					"1h",
					"2h",
					"6h",
					"1d",
					"2d",
					"1week",
					"1month",
					"1trimester",
					"1year"	};

// Minimalist format strings for the intervals above. You can change it.
char *TSL_FormatMinimal[]={	"",	
							"%Mm%Ss",
							"%Mm%Ss",
							"%Mm%Ss",
							"%Hh%Mm",
							"%Hh%Mm",
							"%Hh%Mm",
							"%dd %Hh",
							"%dd %Hh",
							"%dd %Hh",
							"%b-%dd",
							"%b-%dd",
							"%b-%Ww",
							"%Y-%b",
							"%Y-%b",
							"%Y"	};

// Corresponds to the precision of the TSL_FormatMinimal, for labeling the time axis
char *TSL_AxisNameMinimal[]={	"",
								"Time (s)",
								"Time (s)",
								"Time (s)",
								"Time (min)",
								"Time (min)",
								"Time (min)",
								"Time (h)",
								"Time (h)",
								"Time (h)",
								"Date",
								"Date",
								"Week",
								"Month",
								"Month",
								"Year"	};

// Another more complete set of format strings for the intervals above. You can change it or pass whatever you want
char *TSL_FormatTimeDate[]={	"",	
								"%X",
								"%X",
								"%X",
								"%X",
								"%X",
								"%X",
								"%Y-%m-%d\n%X",
								"%Y-%m-%d\n%X",
								"%Y-%m-%d\n%X",
								"%Y-%m-%d",
								"%Y-%m-%d",
								"%Y-%W",
								"%Y-%m-%d",
								"%Y-%m",
								"%Y"	};

// Corresponds to the precision of the TSL_FormatMinimal, for labeling the time axis
char *TSL_AxisNameTimeDate[]={	"",
								"Time",
								"Time",
								"Time",
								"Time",
								"Time",
								"Time",
								"Time",
								"Time",
								"Time",
								"Date",
								"Date",
								"Week",
								"Date",
								"Month",
								"Year"	};

	
///////////////////////////////////////////////////////////////////////////////
typedef struct sTSL {
	int panel, control, 
	X; 					// Position of the last point written (not taking into account offset/gain)
	struct tm *Previous;
} tTSL;
static int NbTSL=0;
static tTSL *TSL=NULL;

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find a strip chart in the allocated list, or add it if not present
/// HIFN	Note that this function doesn't test if the control is a valid strip chart
///////////////////////////////////////////////////////////////////////////////
static tTSL* TSL_Find(int panel, int control) {
	int i;
	for (i=0; i<NbTSL; i++)
		if (TSL[i].panel==panel and TSL[i].control==control)
			return &TSL[i];
	if (NULL==(TSL=realloc(TSL, (size_t)(++NbTSL)*sizeof(tTSL)))) {
		fprintf(stderr, "\nOut of memory for Previous in %s-%s line %d", __FILE__, __func__, __LINE__);
		return NULL;
	}
	tTSL *T=&TSL[NbTSL-1];
	T->panel  =panel;
	T->control=control;
	T->X=0;
	if (NULL==(T->Previous=malloc(sizeof(struct tm)))) {
		fprintf(stderr, "\nOut of memory for Previous in %s-%s line %d", __FILE__, __func__, __LINE__);
		return NULL;
	}
	T->Previous->tm_sec = T->Previous->tm_min = T->Previous->tm_hour = 
	T->Previous->tm_mday= T->Previous->tm_mon = T->Previous->tm_year = 
	T->Previous->tm_wday= T->Previous->tm_yday= T->Previous->tm_isdst= 0;
	
	// Set some sensible defaults
	SetCtrlAttribute (panel, control, ATTR_XAXIS_GAIN,   1.);	// meaningless for times
	SetCtrlAttribute (panel, control, ATTR_XAXIS_OFFSET, 0.);
	SetCtrlAttribute (panel, control, ATTR_XGRID_VISIBLE, FALSE);
	SetCtrlAttribute (panel, control, ATTR_XUSE_LABEL_STRINGS, TRUE);
	SetCtrlAttribute (panel, control, ATTR_SHOW_CHART_DIVISION_LABELS, FALSE);
	
	return T;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Given a rate in seconds, compute an optimal unit interval for the time axis
///	HIFN	This is rather arbitrary and depends on:
/// HIFN	- the rate of updates
/// HIFN	- the font size used for the axis labels
/// HIFN	- the nb of pixels between 2 X points, which itself depends on:
/// HIFN	- the width of the strip chart
/// HIFN	- the number of points per screen
/// HIFN	- If any of the above changes, you should call this again
/// HIFN	If the labels are too close together, they will be set but not displayed
/// HIPAR	panel/Panel of the strip chart
/// HIPAR	control/Control of the strip chart
/// HIPAR	Rate/Expected update rate in seconds (0 for as fast as possible)
/// HIPAR	frmt/Array of 14 format strings corresponding to the various possibilities
/// HIPAR	frmt/such as TSL_FormatMinimal or TSL_FormatTimeDate
/// HIPAR	Adj/Closeness of the labels. 1 by default.
/// HIPAR	Adj/Use more for labels closer together, less for farther apart.
/// HIRET	Return the TSL_NONE..TSL_YEAR recommended interval to pass to TSL_AddTimeStripLabel
///////////////////////////////////////////////////////////////////////////////
int TSL_Auto(int panel, int control, double Rate, char *frmt[], double Adj) {
	int PPS, AW, PixPerPoint, Auto=TSL_NONE;
	double PixPerSec=0;
	int EstimateLabelWidth[]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	static BOOL FirstTime=TRUE;
	int fontSize, bold, italics, underline, strikeout;
	
	if (Rate==0) Auto=TSL_SEC;
	else {
		int i;
		char Label[80], Font[255];
		time_t Time=time(NULL);		// Take a random time, like now
		struct tm *TM=localtime(&Time);
		GetCtrlAttribute (panel, control, ATTR_POINTS_PER_SCREEN, &PPS);
		GetCtrlAttribute (panel, control, ATTR_PLOT_AREA_WIDTH,   &AW);
		PixPerPoint=AW     /PPS;
		PixPerSec  =AW/Rate/PPS;
		for (i=1; i<=TSL_YEAR; i++) {
			strftime (Label, 80, frmt[i], TM);
			// See http://digital.ni.com/public.nsf/allkb/3333698A4A073FD786256DD60077693D
			if (FirstTime) {
				GetCtrlAttribute (panel, control, ATTR_XYNAME_FONT,        Font);
				GetCtrlAttribute (panel, control, ATTR_XYNAME_POINT_SIZE, &fontSize);
				GetCtrlAttribute (panel, control, ATTR_XYNAME_BOLD, 	  &bold);
				GetCtrlAttribute (panel, control, ATTR_XYNAME_ITALIC,     &italics);
				GetCtrlAttribute (panel, control, ATTR_XYNAME_UNDERLINE,  &underline);
				GetCtrlAttribute (panel, control, ATTR_XYNAME_STRIKEOUT,   &strikeout);

				CreateMetaFontWithCharacterSet("ControlFont", Font, fontSize, 
											   bold, italics, underline, strikeout, 0, VAL_NATIVE_CHARSET);
				FirstTime=FALSE;
			}
			GetTextDisplaySize (Label, "ControlFont" /*Font*/, NULL, &EstimateLabelWidth[i]);
		}
		//MinTimeBetweenLabels = EstimateLabelWidth/PixPerSec;
		i=0;
		
#define ADJ(t) EstimateLabelWidth[++i]/PixPerSec<=TSL_MTBL[TSL_##t]*Adj ? TSL_##t
		Auto= (	ADJ(  SEC) :
				ADJ( 3SEC) :
				ADJ(10SEC) :
				ADJ(  MIN) :
				ADJ( 3MIN) :
				ADJ(10MIN) :
				ADJ( HOUR) :
				ADJ(2HOUR) :
				ADJ(6HOUR) :
				ADJ( DAY)  :
				ADJ(2DAY)  :
				ADJ(WEEK)  :
				ADJ(MONTH) : 
				ADJ(TRIM)  : 
				TSL_YEAR	);
	}
#ifdef TSL_DEBUG	
	char Str[80];
	double MinTimeBetweenLabels=0;
	printf("Rate:%f, Suggest:%s, mtbl:%f, pix/s:%f\n", 
		Rate, TSL_Label[Auto], MinTimeBetweenLabels, PixPerSec);
	sprintf(Str, "%s/%.0fpix", TSL_Label[Auto], TSL_MTBL[Auto]*Adj*PixPerSec);
	SetCtrlVal(Pnl, PNL_AUTO, Str);
#endif	
	return Auto;
}
	   

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Possibly add a time label to the X axis of a strip chart
///	HIFN	Call this function right after adding a point to the strip chart with PlotStripChart/PlotStripChartPoint
///	HIFN	You must enable the label strings in the strip chart with SetCtrlAttribute(,,ATTR_XUSE_LABEL_STRINGS,TRUE)
/// HIFN	You can call this function more than once per second
/// OUT		Previous, X
/// HIPAR	panel/The panel of the strip chart
/// HIPAR	control/The control of the strip chart
/// HIPAR	Time/The time of the data point, pass 0 to use the current time
/// HIPAR	Unit/The interval to respect between labels. See TSL_Auto
/// HIPAR	Previous/Pass Previous->tm_year=0 the first call and send back the previous value every time
/// HIPAR	X/Number of times PlotStripChart has been called (position of the last point). Start at 0
/// HIPAR	X/You must reset it to 0 if you change the PPS
/// HIPAR	frmt/Time format, you can use TSL_Format[TSL_Unit] or any strftime string you wish
/// HIRET	TRUE is a new label has just been added
///////////////////////////////////////////////////////////////////////////////
static BOOL TSL_AddTimeStripLabel(int panel, int control, time_t Time, int Unit, struct tm *Previous, 
	int *X, char *frmt) {
	BOOL DoPlot=FALSE;		// Add a label
	struct tm *TM;
	char Label[80];
	
	if (Time==0) Time=time(NULL);
	TM=localtime(&Time);

	if (Unit==TSL_NONE)            DoPlot=FALSE;
	else if (Previous->tm_year==0) DoPlot=TRUE;
	else switch (Unit) {
		#define TDIF(What) (TM->tm_##What != Previous->tm_##What) 
		case TSL_SEC:	DoPlot= memcmp(TM, Previous, sizeof(struct tm))!=0; break;
		case TSL_3SEC:	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TDIF(hour) or TDIF(min) or TM->tm_sec/3  != Previous->tm_sec/3; break;
		case TSL_10SEC:	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TDIF(hour) or TDIF(min) or TM->tm_sec/10 != Previous->tm_sec/10; break;
		case TSL_MIN :	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TDIF(hour) or TDIF(min); break;
		case TSL_3MIN:  DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TDIF(hour) or TM->tm_min/3  != Previous->tm_min/3; break;
		case TSL_10MIN: DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TDIF(hour) or TM->tm_min/10 != Previous->tm_min/10; break;
		case TSL_HOUR:	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TDIF(hour); break;
		case TSL_2HOUR:	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TM->tm_hour/2 != Previous->tm_hour/2; break;
		case TSL_6HOUR:	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday) or TM->tm_hour/6 != Previous->tm_hour/6; break;
		case TSL_DAY:	DoPlot= TDIF(year) or TDIF(mon) or TDIF(mday); break;
		case TSL_2DAY:	DoPlot= TDIF(year) or TDIF(mon) or TM->tm_mday/2 != Previous->tm_mday/2; break;	// or yday ?
		case TSL_WEEK:	DoPlot= TDIF(year) or TM->tm_yday/7 != Previous->tm_yday/7; break;
		case TSL_MONTH:	DoPlot= TDIF(year) or TDIF(mon); break;
		case TSL_TRIM:	DoPlot= TDIF(year) or TM->tm_mon/3 != Previous->tm_mon/3; break;
		case TSL_YEAR:	DoPlot= TDIF(year); break;
	}
	
	if (DoPlot) {
		double Min, Max, Val; 
		strftime (Label, 80, frmt, TM);
		InsertAxisItem     (panel, control, VAL_BOTTOM_XAXIS, -1, Label, *X);
		GetAxisScalingMode (panel, control, VAL_BOTTOM_XAXIS, NULL, &Min, &Max);
		GetAxisItem        (panel, control, VAL_BOTTOM_XAXIS, 0, NULL, &Val);
		if (Val<Min)
			DeleteAxisItem (panel, control, VAL_BOTTOM_XAXIS, 0, 1);	// Keep a manageable amount
#ifdef TSL_DEBUG
		int C; char Str[80];
		GetNumAxisItems (panel, control, VAL_BOTTOM_XAXIS, &C);
		sprintf(Str, "\n%s - %.0f~%.0f - %d labels", Label, Min, Max, C);
		SetCtrlAttribute(Pnl, PNL_LATEST, ATTR_CTRL_VAL, Str);
#endif
	}
	memcpy (Previous, TM, sizeof(struct tm));
	(*X)++;
	return DoPlot;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Free the allocated space (which isn't much, unless you have tens of thousands of charts)
///////////////////////////////////////////////////////////////////////////////
void TSL_Free(void) {
	int i;
	for (i=0; i<NbTSL; i++)
		free(TSL[i].Previous);
	free(TSL);
	TSL=NULL;
	NbTSL=0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Replaces PlotStripChartPoint, but possibly add a time label
/// HIFN	See TSL_AddTimeStripLabel and PlotStripChartPoint for the parameter details
///////////////////////////////////////////////////////////////////////////////
int TSL_PlotStripChartPoint(int panel, int control, double Y, 
		time_t Time, int Unit, char *frmt) {
	tTSL *T=TSL_Find(panel, control);
	int r=PlotStripChartPoint(panel, control, Y);
	TSL_AddTimeStripLabel(panel, control, Time, Unit, T->Previous, &T->X, frmt);
	return r;
}
	
///////////////////////////////////////////////////////////////////////////////
/// HIFN	Replaces PlotStripChart, but possibly add a time label
/// HIFN	See TSL_AddTimeStripLabel and PlotStripChart for the parameter details
///////////////////////////////////////////////////////////////////////////////
int TSL_PlotStripChart(int panel, int control, 
		void* Y, int Nb, int Start, int Skip, int yDataType,	// See PlotStripChart
		time_t Time, int Unit, char *frmt) {					// See TSL_AddTimeStripLabel
	tTSL *T=TSL_Find(panel, control);
	int r=PlotStripChart (panel, control, Y, (size_t)Nb, Start, Skip, yDataType);
	TSL_AddTimeStripLabel(panel, control, Time, Unit, T->Previous, &T->X, frmt);
	return r;
}
	
///////////////////////////////////////////////////////////////////////////////
/// HIFN	Must be called if you clear or change the PPS on the strip
///////////////////////////////////////////////////////////////////////////////
int TSL_Reset(int panel, int control) {
	tTSL *T=TSL_Find(panel, control);
	T->X = 0;
	T->Previous->tm_year = 0;	// Force a label next time
	return ClearAxisItems (panel, control, VAL_BOTTOM_XAXIS);
}




///////////////////////////////////////////////////////////////////////////////
// Below is for testing - Needs the TimeStripLabel.uir user interface file
///////////////////////////////////////////////////////////////////////////////
#ifdef TSL_DEBUG

static int TSL_Unit=TSL_NONE;
static double Rate=1.;			// Expected update rate in seconds

// This is used to speed up time
static time_t RefTime=0;
static int TimeFactor=0, TimeNb=0;


int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "TimeStripLabelTest.uir", PNL)) < 0)
		return -1;
	CallPanelCallback(Pnl,             EVENT_PANEL_SIZE, 0, 0, NULL);
	CallCtrlCallback (Pnl, PNL_PERIOD, EVENT_COMMIT,     0, 0, NULL);
	DisplayPanel (Pnl);

	RunUserInterface ();
	
	DiscardPanel (Pnl);
	return 0;
}

// Enable the printf in TSL_Auto for this
void TSL_Test(void) {
	putchar ('\n');
	#define TEST(TimeInterval) TSL_Auto(Pnl, PNL_STRIPCHART, (TimeInterval), TSL_FormatTimeDate, 1.);
	TEST(0.01);
	TEST(0.02);
	TEST(0.05);
	TEST(0.1);
	TEST(0.2);
	TEST(0.5);
	TEST(1);
	TEST(2);
	TEST(5);
	TEST(10);
	TEST(20);
	TEST(50);
	TEST(1*60);
	TEST(2*60);
	TEST(5*60);
	TEST(10*60);
	TEST(20*60);
	TEST(50*60);
	TEST(1*60*60);
	TEST(2*60*60);
	TEST(5*60*60);
	TEST(10*60*60);
	TEST(20*60*60);
	TEST(1*60*60*24);
	TEST(2*60*60*24);
	TEST(5*60*60*24);
	TEST(10*60*60*24);
	TEST(20*60*60*24);
	TEST(50*60*60*24);
	TEST(100*60*60*24);
	TEST(200*60*60*24);
	TEST(1*60*60*24*365);
	TEST(2*60*60*24*365);
	TEST(5*60*60*24*365);
	putchar ('\n');
}

int CVICALLBACK cbp_TimeStripLabel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Height, Width, Top;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(panel, ATTR_HEIGHT, &Height);
			GetPanelAttribute(panel, ATTR_WIDTH,  &Width);
			GetCtrlAttribute (panel, PNL_STRIPCHART, ATTR_TOP,   &Top);
			SetCtrlAttribute (panel, PNL_STRIPCHART, ATTR_WIDTH,  Width);
			SetCtrlAttribute (panel, PNL_STRIPCHART, ATTR_HEIGHT, Height-Top);
			TSL_Unit=TSL_Auto(panel, PNL_STRIPCHART, Rate+(TimeFactor ? 2<<TimeFactor : 0), TSL_FormatTimeDate, 1.);
			SetCtrlAttribute (panel, PNL_STRIPCHART, ATTR_XNAME, TSL_AxisNameTimeDate[TSL_Unit]);

//			TSL_Test(); CallCtrlCallback (Pnl, PNL_PERIOD, EVENT_COMMIT,     0, 0, NULL);
			break;
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
	}
	return 0;
}

int CVICALLBACK cb_UpdateRate (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Rate);
			SetCtrlAttribute (panel, PNL_TIMER, ATTR_INTERVAL, Rate);
			TSL_Unit=TSL_Auto(panel, PNL_STRIPCHART, Rate+(TimeFactor ? 2<<TimeFactor : 0), TSL_FormatTimeDate, 1.);
			SetCtrlAttribute (panel, PNL_STRIPCHART, ATTR_XNAME, TSL_AxisNameTimeDate[TSL_Unit]);
			break;
	}
	return 0;
}

int CVICALLBACK cbt_Timer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	static double Y[2]={0,0};
	switch (event) {
		case EVENT_TIMER_TICK:
			Y[0]+=(rand()/(double)RAND_MAX)-0.5;
			Y[1]+=(rand()/(double)RAND_MAX)-0.5;
//			TSL_PlotStripChartPoint(Pnl, PNL_STRIPCHART, Y[0], 
//				TimeFactor ? RefTime+TimeNb++*(2<<TimeFactor) : 0,
//				TSL_Unit, TSL_FormatTimeDate[TSL_Unit]);
			TSL_PlotStripChart(Pnl, PNL_STRIPCHART, Y, 2, 0, 0, VAL_DOUBLE,
				TimeFactor ? RefTime + (unsigned long)TimeNb++ * (2UL<<TimeFactor) : 0,
				TSL_Unit, TSL_FormatTimeDate[TSL_Unit]);
			break;
	}
	return 0;
}

int CVICALLBACK cb_PPS (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int PPS;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &PPS);
			SetCtrlAttribute (Pnl, PNL_STRIPCHART, ATTR_POINTS_PER_SCREEN, PPS);
			TSL_Reset(Pnl, PNL_STRIPCHART);
			GetCtrlVal(Pnl, PNL_PERIOD, &Rate);
			TSL_Unit=TSL_Auto(Pnl, PNL_STRIPCHART, Rate+(TimeFactor ? 2<<TimeFactor : 0), TSL_FormatTimeDate, 1.);
			SetCtrlAttribute (Pnl, PNL_STRIPCHART, ATTR_XNAME, TSL_AxisNameTimeDate[TSL_Unit]);
			break;
	}
	return 0;
}

int CVICALLBACK cb_TimeFactor (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[255];
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &TimeFactor);
			RefTime=time(NULL);
			TimeNb=0;
			TSL_Reset(Pnl, PNL_STRIPCHART);
			ClearStripChart  (Pnl, PNL_STRIPCHART);
			TSL_Unit=TSL_Auto(Pnl, PNL_STRIPCHART, Rate+(TimeFactor ? 2<<TimeFactor : 0), TSL_FormatTimeDate, 1.);
			SetCtrlAttribute (Pnl, PNL_STRIPCHART, ATTR_XNAME, TSL_AxisNameTimeDate[TSL_Unit]);
			if (TimeFactor) sprintf(Str, "Speedup (%.0fs/tick)", Rate+(2<<TimeFactor));
			else            sprintf(Str, "Speedup (0=real time)");
			SetCtrlAttribute (Pnl, PNL_TIME_FACTOR, ATTR_LABEL_TEXT, Str);
			break;
	}
	return 0;
}

int CVICALLBACK cb_ScrollMode (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int SM;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &SM);
			SetCtrlAttribute (Pnl, PNL_STRIPCHART, ATTR_SCROLL_MODE, SM);
			break;
	}
	return 0;
}


#endif // DEBUG

