/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2012. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNL                              1       /* callback function: cbp_TimeStripLabel */
#define  PNL_PERIOD                       2       /* control type: numeric, callback function: cb_UpdateRate */
#define  PNL_TIME_FACTOR                  3       /* control type: numeric, callback function: cb_TimeFactor */
#define  PNL_PPS                          4       /* control type: numeric, callback function: cb_PPS */
#define  PNL_SCROLL_MODE                  5       /* control type: ring, callback function: cb_ScrollMode */
#define  PNL_QUIT                         6       /* control type: command, callback function: (none) */
#define  PNL_STRIPCHART                   7       /* control type: strip, callback function: (none) */
#define  PNL_TIMER                        8       /* control type: timer, callback function: cbt_Timer */
#define  PNL_AUTO                         9       /* control type: textMsg, callback function: (none) */
#define  PNL_LATEST                       10      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cb_PPS(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ScrollMode(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_TimeFactor(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_UpdateRate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_TimeStripLabel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbt_Timer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
